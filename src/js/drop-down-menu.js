"use strict"

function dropDown() {
    const btn = document.getElementById("burger-menu"),
        navList = document.getElementsByClassName("navigation__menu")[0];

    btn.addEventListener("click", () => {
        navList.classList.toggle("navigation__menu--active");
        btn.classList.toggle("navigation__btn-active");
    })
}

dropDown();


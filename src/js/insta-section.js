function changeContent() {
    const head = document.getElementsByClassName("instagram__head")[0];

    if (window.screen.availWidth >= 480) {
        head.textContent = "- latest instagram shots";
    }

    window.addEventListener("resize", () => {
        if (window.screen.availWidth >= 480) {
            head.textContent = "- latest instagram shots";
        } else if (window.screen.availWidth < 480) {
            head.textContent = "- last instagram shot";
        }
    })
}

changeContent();


